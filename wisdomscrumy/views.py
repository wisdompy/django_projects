from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ScrumyGoals, ScrumyHistory, GoalStatus
from django.contrib.auth.models import User, Group
import random
from .forms import SignupForm, CreateGoalForm, ChangeGoalStatus,UpdateUserForm
from django.contrib import messages
from django.contrib.auth import login,authenticate
from django.forms.models import model_to_dict


def index(request):
    user = User.objects.get(username = "louis")
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username= form.cleaned_data('username')
            raw_password = form.cleaned_data('password')
            my_group = Group.objects.get(name = 'Developer')
            my_group.user_set.add(user)
            user = authenticate(username=username, password=raw_password)
            login(request,user)
            # username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created, you can now login!')
            return redirect('message')
    else:
        form = SignupForm()

    return render(request, "wisdomscrumy/index.html", {'form':form})
    # context= ScrumyGoals.objects.get(goal_name="Learn Django")
    
    # return HttpResponse(context.goal_name )

def move_goal(request,goal_id ):
    goalnames = ScrumyGoals.objects.get(goal_id=goal_id)
    prev_status = goalnames.goal_status.status_name
    form = ChangeGoalStatus()
    current_user = request.user
    if request.method == 'POST':
        form = ChangeGoalStatus(request.POST, instance=goalnames)
        if form.is_valid():
            if current_user.groups.filter(name='Developer').exists():
                if current_user.username == goalnames.owner and goalnames.goal_status.status_name != 'Done Goal': 
                    form.save()
                    return redirect ('home')
                    messages.success(request, f'Your goals has been successfully updated!!')
                else:
                    goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                    goalid.save()
                    form = ChangeGoalStatus()
                    messages.warning(request, f'You can  move your own goals but not to the Done Goal column!')
                    
                    # context = {
                    #         'form': form,
                    #         'goal_name': goalid,
                    #         'error': 'You can  move your own goals but not to the Done Goal column'
                    # }
                    # return render(request, 'wisdomscrumy/movegoal.html', context)
            elif current_user.groups.filter(name='Quality Assurance').exists():
                if current_user.username == goalnames.owner:
                    form.save()
                    return redirect ('home')
                    messages.success(request, f'Your goals has been successfully updated!!')
                elif prev_status == 'Verify Goal' or prev_status== 'Done Goal':
                    form.save()
                    return redirect ('home')
                    messages.success(request, f'Your goals has been successfully updated!!')
                else:
                    goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                    goalid.save()
                    form = ChangeGoalStatus()
                    messages.warning(request,f'You are only permitted to move goals from/to either a Verify or a Done status if it wasnt created by you')
            elif current_user.groups.filter(name='Owner').exists():  
                if current_user.username == goalnames.owner: 
                    form.save()      
                    return redirect ('home')  
                    messages.success(request, f'Your goals has been successfully updated!!')
                else:
                    goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                    goalid.save()
                    form = ChangeGoalStatus()
                    messages.warning(request, f'You cannot move this goal since you didnt created , You Belong to the Group Owner!!!!')

            elif current_user.groups.filter(name='Admin').exists():
                form.save()
                return redirect ('home')
                messages.success(request, f'Your goals has been successfully updated!!')
            else:
                goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                goalid.save()
                form = ChangeGoalStatus()
                messages.warning(request, f'Your are not permitted to a move a goal, you do not belong to any group!!')
    else:
        form = ChangeGoalStatus()


                
                    

    return render (request, 'wisdomscrumy/movegoal.html', {'form':form})

  

   
def add_goal(request):
    weekly = GoalStatus.objects.get(status_name= "Weekly Goal")
    daily = GoalStatus.objects.get(status_name="Daily Goal")
    goals = [weekly]
    form = CreateGoalForm()
    if request.method == 'POST':
        form = CreateGoalForm(request.POST)
        if form.is_valid():
            f= form.save(commit = False)
            random_id = random.randint(1000,9999) 
            user = form.cleaned_data['user']
            my_user= User.objects.get(username=user)
            f.goal_id= random_id
            f.goal_status=weekly
            f.created_by=my_user
            f.owner=my_user
            f.moved_by=my_user
            f.save()
            return redirect ('home')
    else:
        form = CreateGoalForm()
    context = {'form':form}

    return render (request, 'wisdomscrumy/addgoal.html',context)

    # user = User.objects.get(username = "louis")
    # status = GoalStatus.objects.get(status_name= "Weekly Goal")
    # id_list = []
    # for i in range(1000,9999):
    #     random_id = random.randint(1000,9999)
    #     if random_id not in id_list:
    #         id_list.append(random_id)
    #         my_goal = ScrumyGoals.objects.create(goal_name="Keep Learning Django", goal_id=random_id, user=user, goal_status=status,created_by="Louis", moved_by= "Louis",owner="Louis",)
            # my_goal.save()
   
 
        #   return HttpResponse(my_goal.goal_name)

def home(request, *args, **kwargs):
    user = User.objects.all()
    current_user = request.user
    # related_user= user.ScrumyGoals_set.all()
    WeeklyGoals = GoalStatus.objects.get(status_name= "Weekly Goal")
    related_weekly_goal = WeeklyGoals.scrumygoals_set.all()
    DailyGoals =  GoalStatus.objects.get(status_name= "Daily Goal")
    related_daily_goal= DailyGoals.scrumygoals_set.all()
    DoneGoal =  GoalStatus.objects.get(status_name= "Done Goal")
    related_done_goal= DoneGoal.scrumygoals_set.all()
    VerifyGoal= GoalStatus.objects.get(status_name= "Verify Goal")
    related_verify_goal= VerifyGoal.scrumygoals_set.all()
    
    
    # goal_list = ScrumyGoals.objects.filter(goal_name = "Keep Learning Django")
    # context = ScrumyGoals.objects.all()
    # container = ' , '.join([g.goal_name for g in goal_list])
    dictionary =  {'user': user, 
                   'related_weekly_goal': related_weekly_goal,     
                   'related_daily_goal': related_daily_goal,
                   'related_done_goal' : related_done_goal,
                   'related_verify_goal' : related_verify_goal,
    }
    

    return render(request, "wisdomscrumy/home.html", dictionary)

    
def message(request):
    return render (request, "wisdomscrumy/message.html")


 # try:
    #     
    # except Exception as e:

    #     return render(request, "wisdomscrumy/exception.html", {'error':'A record with that goal id does not exist'})
    # else:
        # return HttpResponse(goal.goal_name)
