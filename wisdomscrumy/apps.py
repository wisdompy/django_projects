from django.apps import AppConfig


class WisdomscrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wisdomscrumy'
