from django.db import models
from django.contrib.auth.models import User,Group,Permission

# Create your models here.

class ScrumyGoals(models.Model):
    goal_name = models.CharField(max_length = 100)
    goal_id = models.IntegerField(default=0)
    created_by = models.CharField(max_length= 50)
    moved_by = models.CharField(max_length=50)
    owner = models.CharField(max_length= 50)
    goal_status = models.ForeignKey('GoalStatus', on_delete=models.PROTECT, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name = 'ScrumyGoals_set', null=True)

    class Meta:
        permissions = [
            ("create_new_Weekly_goal"," Can create a new Weekly Goal for a user"),
            ("change_weekly_goal_to_daily_goal", "Can change Weekly Goal to Daily Goal"),
            ("change_goal_status_to_done_goal", "can change goal status to Done Goal"),
            ("change_goal_status_to_verify_goal", "can change goal status to Verify Goal"),
            ("change_goal_status_to_daily_goal", "Can change goal status to Daily Goal"),
            ("change_goal_status_to_weekly_goal", "can change goal status to Weekly Goal"),
            

        ]
            
        

    def __str__(self):
        return self.goal_name


class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length=50)
    created_by = models.CharField(max_length=50)
    moved_from = models.CharField(max_length =50)
    moved_to = models.CharField(max_length= 50)
    time_of_action = models.DateField()
    goal = models.ForeignKey('ScrumyGoals', on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by
    
class GoalStatus(models.Model):
    status_name = models.CharField(max_length=50)

    def __str__(self):
        return self.status_name



