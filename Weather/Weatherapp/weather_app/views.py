from django.shortcuts import render
import urllib.request
import json

# Create your views here.

def index(request):
    if request.method=="POST":
        city = request.POST['city']
        source = urllib.request.urlopen('http://api.openweathermap.org/data/2.5/weather?q='+ city + '&units=metric&appid=b970de490f6b994a82d15625554ad53a').read()
        data_list = json.loads(source)

        data ={
            'country_code':str(data_list['sys']['country']),
            'coordinate': str(data_list['coord']['lon']),
            'temp': str(data_list['main']['temp']),
            'pressure': str(data_list['main']['pressure']),
            'humidity': str(data_list['main']['humidity']),
            'main': str(data_list['weather'][0]['main']),
            'description':str(data_list['weather'][0]['description']),
            'icon':data_list['weather'][0]['icon']
        }
        print(data)
    else:
        data={}
    return render(request, 'main/index.html', data)