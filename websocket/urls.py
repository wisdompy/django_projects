from django.urls import path

from . import views



urlpatterns = [
    path('test/', views.test, name='test'),
    path('disconnect/', views.disconnect_view, name= "disconnect"),
    path('connect/', views.connect_view, name= "connect"),
    path('sendmessage/', views.send_message, name='sendmessage'),
    path('getmessage/', views.get_recent_message, name='getmessage'),
]