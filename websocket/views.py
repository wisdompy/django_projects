from django.core.checks import messages
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json 
from .models import ChatMessage, Connection
import boto3


@csrf_exempt
def test(request):
    return JsonResponse({'message': 'hello Daud'}, status=200)
@csrf_exempt
def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)
@csrf_exempt
def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client('apigatewaymanagementapi', endpoint_url='',
                               region_name='us-east-2', aws_access_key_id='', aws_secret_access_key= '')
    return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))
@csrf_exempt
def connect_view(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connect = Connection(connection_id=connection_id)
    connect.save()
    return JsonResponse({'message': 'connect successfully'}, status=200)

@csrf_exempt
def disconnect_view(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    disconnect = Connection.objects.get(connection_id=connection_id)
    disconnect.delete()
    return JsonResponse({'message':'disconnection successful'}, status=200)
@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    username = body['username']
    content= body['content']
    timestamp = body['timestamp']
    chat = ChatMessage(username=username, messages= content, timestamp=timestamp)
    chat.save()
    connections = Connection.objects.all()
    data = {'messages':[body]}
    for connection in connections:
        print(connection)
        _send_to_connection(connection.connection_id, data)
    return JsonResponse({'message':'messages has been sent to all connections'}, status=200)
@csrf_exempt
def get_recent_message(request):
    messages = []
    chats = ChatMessage.objects.all().order_by("-id")
    for i in range(len(chats)):
        data = {
            'username': chats[i].username,
            'message': chats[i].messages,
            'timestamp': chats[i].timestamp
        }

        messages.append(data)

    data = {'messages':messages}
    for connection in Connection.objects.all():
        _send_to_connection(connection.connection_id, data)
        return JsonResponse(data, status=200)
