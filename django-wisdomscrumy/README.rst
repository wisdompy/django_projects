====
wisdomscrumy
====
wisdomscrumy is a simple django project that prints welcome to django on your page

Detailed info is in the docs directory

1. Add "wisdomscrumy" to INSTALLED_APPS in settings like this
    INSTALLED_APPS = [
        ....,
        'wisdomscrumy'
    ]

2.Include wisdomscrumy in the URLconfig in your projects folder urls.py like this

    path('wisdomscrumy/, include('wisdomscrumy.urls'))

3. run python manage.py migrate

4. run the development server - python manage.py runserver

5. visit http://127.0.0.1:8000/wisdomscrumy