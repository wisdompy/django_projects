import os

from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README =readme.read()

#allow setup.py to run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__),os.pardir)))

setup(
    name = 'django-wisdomscrumy',
    version = '0.1',
    packages = find_packages(),
    include_package_data= True,
    license = 'BSD license',
    description = 'Sample installable django app',
    long_description =README,
    url = 'https://www.examle.com/',
    author = 'wisdom',
    author_email = 'idriswisdom6@gmail.com',
    
    classifiers =[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework:: X.Y', # replace X.Y as appropriate 
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9 :: CPython',
        'Topic :: Internet :: WWW/HTTP ',
        'Topic :: Internet :: WWW/HTTP :: Dyanmic content',
    ],
)
